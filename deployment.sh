#!/bin/bash

# Variables
resource_group="OCC_ASD_Lucas"
vnet_name="Vnet_OCC_ASD_Lucas"
location="francecentral"
vnet_address_prefix="10.0.8.0/24"
security_group_name="docker-lucasNSG"
vm_name="docker-lucas"
vm_size="Standard_B2s"
admin_user="lucas"
docker_compose_file="docker-compose.yml"

# Fonction pour vérifier si la ressource existe
resource_exists() {
    local resource_type=$1
    local resource_name=$2
    az $resource_type show --name $resource_name --resource-group $resource_group &> /dev/null
}

# Création du groupe de ressources
if ! resource_exists "group" "$resource_group"; then
    az group create --name $resource_group --location $location || { echo "Erreur lors de la création du groupe de ressources."; exit 1; }
fi

# Création du groupe de sécurité
if ! resource_exists "network nsg" "$security_group_name"; then
    az network nsg create --resource-group $resource_group --name $security_group_name --location $location || { echo "Erreur lors de la création du groupe de sécurité."; exit 1; }
fi

# Autoriser le trafic SSH
az network nsg rule create \
    --resource-group $resource_group \
    --nsg-name $security_group_name \
    --name allow_ssh \
    --priority 1001 \
    --direction Inbound \
    --source-address-prefix '*' \
    --source-port-range '*' \
    --destination-address-prefix '*' \
    --destination-port-range 22 \
    --access Allow \
    --protocol Tcp \
    --description "Allow SSH traffic" || { echo "Erreur lors de l'autorisation du trafic SSH."; exit 1; }

# Création du réseau virtuel
if ! resource_exists "network vnet" "$vnet_name"; then
    az network vnet create \
        --resource-group $resource_group \
        --name $vnet_name \
        --location $location \
        --address-prefix $vnet_address_prefix \
        --subnet-name admin \
        --subnet-prefix 10.0.8.0/28 \
        --network-security-group $(az network nsg show --resource-group $resource_group --name $security_group_name --query id --output tsv) || { echo "Erreur lors de la création du réseau virtuel."; exit 1; }
fi

# Création de la machine virtuelle
if ! resource_exists "vm" "$vm_name"; then
    az vm create \
        --resource-group $resource_group \
        --name $vm_name \
        --image 'Debian11' \
        --size $vm_size \
        --admin-username $admin_user \
        --ssh-key-value 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDPH2XKskALS5p8L5sHgnlgOW22IW6kdJoTQ7vSZAClF4Y77QYEuQ9fyALUCQj2WutHr036hxvPcVc8ZkIg4VaXXegDZ+XaxQci5Mw3aKaj4Mf4rSs11DEWO0V/1RN+o3Tpo433t7EmHJidvQJ1d8xkt8YI22DZs/xPVa1ORw1kWNe++BuZbDj9Jo4mTja0c7Ib2JxlAJHeuL5KTYhVagGIQtKePt5+mX2fNttRh4l9mauiZrLPbzkSr2+bvLq74rtkWFGMCP3+Bn49jsdTKOeLGGyGxF6eRIPpSsR4b1rNeDgHgtCLRf4Nw9kpuYAJSVR8AlOcL2iT+ee1j0aqtZALqB3gyWg7tMSGLnHWqYKnw4YjdPr0RICSx3NnonMGScpVv7GjxyDkFj7Qfx7G0KSm6gvSGN3EucE71/J/RgskWjCi8239e9SbKj4l77CJ2QiKLE3r3B9cS7WFKc+eR1Jfiobj9dnA4UgAK6Q6IR3ifFzfHORvHYk4qkXlgJcw+VU= generated-by-azure' \
        --vnet-name $vnet_name \
        --subnet admin || { echo "Erreur lors de la création de la machine virtuelle."; exit 1; }
else
    echo "La machine virtuelle '$vm_name' existe déjà."
fi

# Création du fichier docker-compose.yml sur la machine virtuelle
#ssh $admin_user@$(az vm show -d -g $resource_group -n $vm_name --query publicIps -o tsv) 'cat <<EOF > ~/docker-compose.yml
#version: "3.1"
#services:
 # db:
  #  image: postgres
   # container_name: db
    #environment:
     # - POSTGRES_DB=postgres
      #- POSTGRES_USER=odoo
      #- POSTGRES_PASSWORD=odoo
    #restart: always
    #volumes:
     # - odoo-db-data:/var/lib/postgresql/data

  #web:
  #  image: odoo:16.0
  #  container_name: odoo
  #  depends_on:
  #    - db
  #  ports:
  #    - "80:8069"
  #  volumes:
  #    - odoo-web-data:/var/lib/odoo
  #    - odoo-config:/etc/odoo
  #    - odoo-addons:/mnt/extra-addons
#EOF
#sudo docker-compose -f ~/docker-compose.yml up -d
#sudo reboot'