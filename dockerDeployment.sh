#!/bin/bash

sudo apt update && sudo apt upgrade -y
    sudo apt install -y docker-compose
    sudo apt install -y docker.io
    sudo usermod -aG docker lucas
    sudo docker network create 'dockerNetwork'
    sudo cat <<EOF > docker-compose.yml
    version: '3.1'
    services:
      db:
        image: postgres
        container_name: db
        environment:
          - POSTGRES_DB=postgres
          - POSTGRES_USER=odoo
          - POSTGRES_PASSWORD=odoo
        restart: always
        volumes:
          - odoo-db-data:/var/lib/postgresql/data

      web:
        image: odoo:16.0
        container_name: odoo
        depends_on:
          - db
        ports:
          - '80:8069'
        volumes:
          - odoo-web-data:/var/lib/odoo
          - odoo-config:/etc/odoo
          - odoo-addons:/mnt/extra-addons
        environment:
          - PGHOST=db
          - PGUSER=odoo
          - PGPASSWORD=odoo
          - PGDATABASE=odoo
        restart: always

    volumes:
      odoo-db-data:
      odoo-web-data:
      odoo-config:
      odoo-addons:
EOF
    sudo docker-compose -f docker-compose.yml up -d
    sudo reboot
