Pour déployer rapidement la configuration d'une infrastructure depuis Azure CLI et exécuter une application Docker, nous allons suivre les étapes suivantes :

- Sur notre Cloud Shell Azure, nous créons le script deployment.sh présent dans le gitlab puis on le lance. Cela va créer le groupe de sécurité avec une règle pour autoriser la connexion SSH, le réseau, les sous réseaux et la VM dans le groupe de ressource.

- Ensuite on lance la VM depuis mobaXterm, on s'y connecte en SSH, on créé le script dockerDeployment présent dans le gitlab et on le lance

#Je voulais faire qu'un seul script qui deploit la machine, installe docker et créé le docker-compose mais je n'y suis pas parvenu dans le temps imparti